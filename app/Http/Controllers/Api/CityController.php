<?php

namespace App\Http\Controllers\Api;

use App\City;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CityController extends Controller
{
    public function index()
    {
      return response(City::all()->toArray(), Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        $city = new City();
        $openWeatherData = ($city->getOpenWeatherData($request->name));
        if(!empty($openWeatherData))
        {
            $city->name = $request->name;
            if($request->filled('refresh_interval')) {
                $city->refresh_interval = $request->refresh_interval;
            }
            $city->temperature = $openWeatherData['temperature'] ?? null;
            $city->weather = $openWeatherData['weather'] ?? null;
            $city->image = $openWeatherData['image'] ?? null;
            $city->save();
            return response($city, Response::HTTP_CREATED);
        }
        return response(['message' => 'Unable to create because we can\'t find the city in open weather API.'], Response::HTTP_NOT_FOUND);
    }

    public function update($id, Request $request)
    {
        $city = City::find($id);
        if($city && $openWeatherData = $city->getOpenWeatherData($city->name)) {
            $city->temperature = $openWeatherData['temperature'] ?? null;
            $city->weather = $openWeatherData['weather'] ?? null;
            $city->image = $openWeatherData['image'] ?? null;
            $city->updated_at = Carbon::now();
            $city->save();
            return response($city, Response::HTTP_OK);
        }
        return response(['message' => 'Not Found'], Response::HTTP_NOT_FOUND);
    }

    public function destroy($id, Request $request)
    {
        City::destroy($id);
        return response(null, Response::HTTP_NO_CONTENT);
    }
}