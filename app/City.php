<?php

namespace App;

use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;
use Exception;
use Illuminate\Support\Facades\Log;

class City extends Model
{
    public function getOpenWeatherData(string $name) : array
    {
        try{
            $openWeatherConfig = config('services.openweather');
            $client = new Client();
            $request = $client->get($openWeatherConfig['url'], [
                'query' => [
                    'q' => $name,
                    'appid' => $openWeatherConfig['app_id'],
                    'units' => $openWeatherConfig['units'],
                ]
            ]);
            $openWeatherData = $request->getBody()->getContents() ?? '{}';
            $openWeatherData = json_decode($openWeatherData, true);
            if(!empty($openWeatherData)) {
                return [
                    'temperature' => $openWeatherData['main']['temp'] ?? 0,
                    'weather' => $openWeatherData['weather'][0] ? $openWeatherData['weather'][0]['main'] : "",
                    'image' => $openWeatherData['weather'][0] ? $openWeatherConfig['image'].'/'.$openWeatherData['weather'][0]['icon'].'@2x.png' : "",
                ];
            }
            return [];
        }
        catch(Exception $e){
            Log::info('Failed to call open weather API');
            return [];
        }
    }
}
