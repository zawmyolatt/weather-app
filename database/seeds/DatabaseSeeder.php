<?php

use App\City;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('cities')->delete();
        factory(City::class)->create([
            'name' => 'Yangon',
            'temperature' => 25,
            'weather' => 'Rain',
            'image' => 'http://openweathermap.org/img/wn/10n@2x.png'
        ]);
        factory(City::class)->create([
            'name' => 'Singapore',
            'temperature' => 26.16,
            'weather' => 'Clouds',
            'image' => 'http://openweathermap.org/img/wn/04n@2x.png'
        ]);
        factory(City::class)->create([
            'name' => 'Mandalay',
            'temperature' => 25.62,
            'weather' => 'Rain',
            'image' => 'http://openweathermap.org/img/wn/10n@2x.png'
        ]);
    }
}
