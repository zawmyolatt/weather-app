<p align="center"><img src="https://proxsoftwaresolution.com/uploads/images/weather.png"></p>

## Requirements

- PHP >= 7.2.0, since that is the requirement of Laravel 6
- Install latest composer version at here : https://getcomposer.org/download
- Install latest node version at here : https://nodejs.org
- Server should meets this requirements : https://laravel.com/docs/6.x#server-requirements

## Installation
- Download Weather App
```
git clone https://gitlab.com/zawmyolatt/weather-app.git
cd weather-app/
composer install
npm install
npm run dev
```
- Set values in .env
```
DB_DATABASE=your_database_name
DB_USERNAME=your_database_username
DB_PASSWORD=your_database_password

OPENWEATHER_URL=http://api.openweathermap.org/data/2.5/weather
OPENWEATHER_APP_ID=ca3cd2075df64125fce0a92914ac0704 #author_app_id
OPENWEATHER_UNITS=metric
```
- Migrating data
```
cd weather-app/ #no need to run if already in this folder
php artisan migrate --seed
```

**Running Demo**

<p align="center"><img src="https://proxsoftwaresolution.com/uploads/images/weather_app.png"></p>

## About This Repo For Weather App With Vue

The user interface should consist of two parts:
- A horizontal list of tiles consisting of
    - the name of the given city
    - an image related to the current weather situation (rain cloud, shining sun etc)
    - the weather data (temperature, situation, forecast)
    - a 'remove' button visible on hover to remove the tile from the list
- The tile creation form consist of
    - An input field that takes a city name for a new tile
    - A select control to specify the refresh interval for a new tile
    - A button to create a new tile and add it to the list of tiles

All tiles need to refresh independently in their given intervals.
The tile list should always use the maximum width possible to fit in all tiles, and all tiles should have
the same width.
If a tile is added or removed, the rest should automatically resize accordingly.
You can set an arbitrary limit for how many tiles can be displayed at the same time, depending on
your layout.
The weather data could be taken from http://openweathermap.org/current, but you can choose a
different service if you prefer.

## License

The Weather App is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).